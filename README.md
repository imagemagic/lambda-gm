# lambda-gm
Test apps for gm node.js library on AWS Lamba.


## AWS documentation

* [Tutorial: Using AWS Lambda with Amazon S3](http://docs.aws.amazon.com/lambda/latest/dg/with-s3-example.html)
* [Using Packages and Native nodejs Modules in AWS Lambda](https://aws.amazon.com/blogs/compute/nodejs-packages-in-lambda/)
* [Class: AWS.Lambda ](http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html)

